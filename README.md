# MQTT HASS BASE

Small python lib to create quickly daemons which interact with HomeAssistant through MQTT.

[![Latest Release](https://gitlab.com/ttblt-hass/mqtt-hass-base/-/badges/release.svg)](https://gitlab.com/ttblt-hass/mqtt-hass-base/-/releases)
[![pipeline status](https://gitlab.com/ttblt-hass/mqtt-hass-base/badges/master/pipeline.svg)](https://gitlab.com/ttblt-hass/mqtt-hass-base/-/commits/master)
[![coverage report](https://gitlab.com/ttblt-hass/mqtt-hass-base/badges/master/coverage.svg)](https://gitlab.com/ttblt-hass/mqtt-hass-base/-/commits/master)

## Documentation

**TODO**
