"""Errors module."""


class MQTTHassBaseError(Exception):
    """Base error class."""
